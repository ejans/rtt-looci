Dummy OROCOS component to test OROCOS-Looci communication.
==========================================================

The orocos_client script (simulating a OROCOS component) connects to a
port with the goal to communicate data via Looci to a remote
component. Here the "looci-server" scripts simulates the server.

The client script then sends a number of fixed size messages (actually
just a number encoded as string) to the server, that communicates this
message to the peer side.

From the Orocos component perspective we can then easily replace the
communication between two Communication Latency Monitor components via
the ROS middleware with the socket communication (going over Looci).

When used with [Comp_TCPServer] the LooCI bundle connects locally to 
this lua script.

Requirements:

  - lua-5.1: `$ sudo apt-get install lua5.1 liblua5.1-0-dev gcc make`
  - luasocket: 
```bash
  $ cd ~/
  $ git clone https://github.com/diegonehab/luasocket.git
  $ cd luasocket
  $ vi src/makefile (edit line 55 to: LUAINC_linux?=$(LUAINC_linux_base)/lua$(LUAV))
  $ make
  $ sudo make install
```
[Comp_TCPServer]: https://bitbucket.org/ejans/comp_tcpserver
