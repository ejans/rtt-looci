local socket = require "socket"
local host = "127.0.0.1"
local port = 33033
local msglen = 9

local fmtstr="%."..tostring(msglen).."u"

local i = 0
local max = 1000

print("Connecting to Looci @ "..host..":"..tostring(port))

local s,err = socket.connect(host, port)

if not s then
   print("Failed to connect: "..err)
   os.exit(1)
end

-- send data to Looci
while true do
   --print("sending  ",i)
   --local idx, err = s:send(string.format(fmtstr, i))

   if err=='closed' then
      print("connection closed by server")
      break
   end
   if err=='timeout' then print("sending timed out") end

   local data, err = s:receive(msglen)
   if err=='closed' then
      print("connection closed by server")
      break
   end
   if err=='timeout' then print("receiving timed out") end

   --local num = tonumber(data)
   --print("received ", num)
   print("received ", data)

   --i = (num+1)%max
end
